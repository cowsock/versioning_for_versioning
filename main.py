from __future__ import print_function, unicode_literals
from visualize_versions import Session_Data
from utils import data_dir, get_filename_by_number, Interactive_Err

import datetime


def help_message():
	return """Commands:
	General:

	h or ? or help: print this list
	q or quit: quit the application

	ls: list input files and their number references

	Version Commands:

	Format:
		file_number    mode    (mode_arguments, ...)

	Modes:
		t or time:
			(description)
			Args:
				increment -> integer (in seconds)

		l or length:
			(description)
			Args:
				threshold -> positive integer
				(polarity) -> -1, 0, 1

		w or words:
			(description)
			Args:
				threshold -> positive integer
				(polarity) -> -1, 0, 1 

		s or sents:
			(description)
			Args:
				threshold -> positive integer
				(polarity) -> -1, 0, 1

		You can specify any combination of the conditions

		Ex: 0 sents 10 1 words 100 1 words 1 -1 time 600

		"From file 0, output a version after every increase of 2 sentences, of 50 words, 
		every decrease of 1 word, and every 10 minutes"  

	"""

def main_loop(generator):
	for i, elt in enumerate(generator):
		dur, text = elt[0], elt[1]
		sec = (dur[1] - dur[0]) / 1000
		print("\nVERSION: {0}, Elapsed time: {1}\n----\n".format(i, datetime.timedelta(seconds=sec)))
	 	print(text)


def check_int(input_str):
	try:
		return int(input_str)
	except ValueError:
		raise Interactive_Err("Error: argument not an integer")

def check_pos_int(input_str):
	num = check_int(input_str)
	if num <= 0:
		raise Interactive_Err("Error: argument not positive")
	return num

def get_threshold_polarity(arg0, arg1):
	arg0 = check_pos_int(arg0)
	if arg1 is not None:
		try:
			arg1 = check_int(arg1)
		except Interactive_Err:
			arg1 = None
	return arg0, arg1


if __name__ == "__main__":
	print("Enter a command below or type 'h' for help.\nEx: 1 time 120")
	while True:
		try:
			u_in = raw_input("\n->")
			if u_in == 'q' or u_in == 'quit':
				break
			elif u_in == 'h' or u_in == 'help' or u_in == 'help':
				print(help_message())
				continue
			elif u_in == "ls":
				print(data_dir())
				continue
			# otherwise, parse the input string as a full-command
			command = u_in.split()
			if len(command) < 2:
				raise Interactive_Err("Not a valid command! (type h for help)")
			try:
				fileNum= int(command[0])
			except ValueError:
				raise Interactive_Err("First argument must be a valid file number")

			
	
			session_data = Session_Data(get_filename_by_number(fileNum))


			modes = command[1:] 

			# we're gonna assemble a function to give to the main loop

			funcs = []
			args = []
			i = 0
			while i < len(modes):
				try:
					if modes[i] == 't' or modes[i] == 'time':
						funcs.append("time")
						args.append((check_pos_int(modes[i+1]) * 1000, 1)) # conversion to milliseconds
						i += 1 # doesn't have third argument
					else:
						if i + 2 < len(modes):
							polarity_arg = modes[i+2]
						else:
							polarity_arg = None
						if modes[i] == 'l' or modes[i] == 'len':
							funcs.append("len")
						elif modes[i] == 'w' or modes[i] == 'words':
							funcs.append("words")
						elif modes[i] == 's' or modes[i] == 'sents':
							funcs.append("sents")
						else:
							raise Interactive_Err("Invalid Mode")
						
						threshold, polarity = get_threshold_polarity(modes[i+1], polarity_arg)
						args.append((threshold, polarity))
						if polarity is not None:
							i += 2
						else:
							i += 1
						
				except IndexError:
					raise Interactive_Err("Non-optional argument required")
				i += 1 # for the command name

			func = session_data.build_fn(funcs, args)

			main_loop(func)

		except Interactive_Err as e:
			print(e.value)
