# Versioning for Logging Data

Class: NLP in Writing Processes

Term: WS 16/17

Lecturer: Cerstin Mahlow

Contributors: Chris Jenkins and Janis Pagel

## Usage

```sh
python2 main.py
```

then follow the instructions on the screen. Type *h* or *?* for a help page.

You can add your own data, just make sure to put the .idfx files into the data folder. The functions in util.py can be used to preprossed an .idfx file in order to get rid of non-xml coding that may occur in .idfx files.
