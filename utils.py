from __future__ import print_function, unicode_literals
import os
import re

data_location = os.path.abspath(os.getcwd()) + "/data/"  

backspace_value = "__BACKSPACE__"

class Interactive_Err(Exception):
	def __init__(self, value):
		self.value = value
	def __str__(self):
		return repr(self.value)


def _preprocess_help(file):
	cleaned = []
	pattern = re.compile(r"\s*<value>&#x8;</value>\s*")
	replacement = "\t<value>__BACKSPACE__</value>\n"
	for line in file:
		match = pattern.search(line)
		if match:
			cleaned.append(replacement)
		else:
			cleaned.append(line)
	with open(file.name[:-4] + "_preprocessed.idfx", 'w') as f:
		f.write("".join(cleaned))

def preprocess():
	for filename in os.listdir(data_location): 
		with open(data_location + filename) as f:
			print("cleaning file: ", filename)
			_preprocess_help(f)


def data_dir():
	"""Returns enumerated listing of data files in string format"""
	start = data_location + ":\n"
	strings = []
	for i, filename in enumerate(sorted(os.listdir(data_location))):
		strings.append(unicode(i) + ": " + filename)
	return start + "\n".join(strings)

def get_filename_by_number(num):
	ls = sorted(os.listdir(data_location))
	try:
		return ls[num]
	except IndexError: # just to have our own message
		raise Interactive_Err("File: " + unicode(num) + " doesn't exist!")


