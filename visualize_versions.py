from __future__ import print_function, unicode_literals
import nltk # for tokenization and sentence splitting
import pattern.de # for POS tagging and parsing
import string
from utils import data_location
from reading_idfx import get_event_stream, KeyboardEvent


class Session_Data(object):
	"""Contains all (relevant) session data. Provides operations to visualize it"""
	def __init__(self, filename):
		es = get_event_stream(filename)
		self.meta = es[0]
		self.events = es[1]
		self.end_time = self.meta.end_time



	def _get_version_by_pred(self, inits, preds, skip_conditions):
		"""All parameters are lists, corresponding to each mode for producing versions
		   inits: Initial values
		   preds: Not accurately named. Functions to obtain a metric from events
		   """
		# all parameters should be lists
		def get_version(self, thresholds, polarities):
			"""thresholds: value at which a the text is different enough to warrant a version
			   polarities: -1 or 1 are multiplied by the threshold. 0 means absolute value."""
			signs = [0] * len(polarities)
			for i, polarity in enumerate(polarities):
				if polarity is None:
					continue
				elif polarity > 0:
					signs[i] = 1
				elif polarity < 0:
					signs[i] = -1

			next_vals = inits
			for i in range(1, len(self.events)):
				skip = [cond(self.events[i]) for cond in skip_conditions]
				if True in skip:
					continue
				diffs = []
				for pred, sign, next_val in zip(preds, signs, next_vals):
					if sign:
						diffs.append((pred(self.events[:i]) - next_val) * sign)
					else:
						diffs.append(abs(pred(self.events[:i]) - next_val) )
				limits = [diff >= threshold for diff, threshold in zip(diffs, thresholds)]
				if True in limits:
					yield get_duration(self.events[:i]), get_text(self.events[:i])
					next_vals = [pred(self.events[:i]) for pred in preds]

			# output the final document
			yield get_duration(self.events), get_text(self.events)
		return get_version



	def build_fn(self, types, args):
		inits = []
		preds = []
		skip_conditions = []
		thresholds = [arg[0] for arg in args]
		polarities = [arg[1] for arg in args]
		for t in types:
			if t == "time":
				inits.append(self.events[0].start_time)
				preds.append(time_pred)
				skip_conditions.append(lambda e: False)
			elif t == "len":
				inits.append(1)
				preds.append(doclen_pred)
				skip_conditions.append(lambda e: False)
			elif t == "words":
				inits.append(0)
				preds.append(word_pred)
				skip_conditions.append(word_skip)
			elif t == "sents":
				inits.append(0)
				preds.append(sent_pred)
				skip_conditions.append(sent_skip)
			else:
				assert(False)
		return self._get_version_by_pred(inits, preds, skip_conditions)(self, thresholds, polarities)



def get_text(sequence):
		"""Iterates over sequence of events, returns resulting text"""
		char_list = [] # this is gonna be a list of length 1 strings, as if it was a low-level string
		for event in sequence:
			event.apply_text(char_list)
		return "".join(char_list)


def get_duration(sequence):
	"""Looks at sequence of events, returns start and end times"""
	if not len(sequence):
		return (0, 0)
	elif len(sequence) == 1:
		return (sequence[0].start_time, sequence[0].end_time)
	else:
		return (sequence[0].start_time, sequence[-1].end_time)

def time_pred(events):
	return events[-1].start_time

def doclen_pred(events):
	return events[-1].doc_len

def word_pred(events):
	text = get_text(events)
	return len([w for w in tokens(text) if w not in list(string.punctuation)])

def word_skip(e):
	return not isinstance(e, KeyboardEvent) or not e.value or not e.value.isspace()

def sent_pred(events):
	text = get_text(events)
	return len([s for s in sent(text) if tokens(s)[-1] in ('.','!','?', ':') ])

def sent_skip(e):
	return not isinstance(e, KeyboardEvent) or not e.value or not e.value.isspace()

def tokens(chunk):

	return nltk.word_tokenize(chunk)

def sent(chunk):

	return nltk.sent_tokenize(chunk)

def pos(chunk):

	return attern.de.parse(chunk)

def parse(chunk):

	return pattern.de.parsetree(chunk)
