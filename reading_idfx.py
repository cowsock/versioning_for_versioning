from __future__ import print_function, unicode_literals
import os
import re


import xml.etree.ElementTree as ET

from utils import data_location # path where data is kept

from utils import backspace_value # string constant


class Session_Meta(object):
	def __init__(self, name_, language_, age_, gender_, session_, end_time_):
		self.name = name_
		self.language = language_
		self.age = age_
		self.gender = gender_
		self.session = session_
		self.end_time = end_time_

	def __str__(self):
		return "Participant: {}\nLanguage: {}\nAge: {}\nGender: {}\nSession: {}".format(
			self.name, self.language, self.age, self.gender, self.session)

class Event(object):
	def __init__(self, id_, start_time_, end_time_):
		self.id = id_
		self.start_time = int(start_time_)
		self.end_time = int(end_time_)
		if self.end_time < self.start_time:
			self.end_time = self.start_time

	def __str__(self):
		return "id: {}\nTime: {}-{}".format(self.id, self.start_time, self.end_time)

	def get_duration(self):
		if self.start_time == None:
			raise ValueError("This Event does not have a time, refer to its id instead")

		# NOTE: in KeyboardEvents with backspace, this duration might cause an error!!!
		return self.end_time - self.start_time

	def apply_text(self, char_list):
		raise NotImplementedError # purposeful - only defined by subclasses


class KeyboardEvent(Event):
	"""A KeyboardEvent occurring within the context of quote-unquote INPUT MODE of MS Office Word.
	   This means that it should not be compatable with KeyboardEvents from input log that happen
	   in the context of any other window focus (where they would have no position or doc length values)"""
	def __init__(self, id_, start_time_, end_time_, position_, doc_len_, key_, value_=None, keyboardstate_=None):
		super(KeyboardEvent, self).__init__(id_, start_time_, end_time_)
		self.position = int(position_)
		self.doc_len = int(doc_len_)
		self.key = key_
		self.value = value_
		self.keyboardstate = keyboardstate_

	def __str__(self):
		return type(self).__name__ + "\n" + super(KeyboardEvent, self).__str__() + "\nPosition: {}\nDoc Length: {}\nKey: {}\nValue: {}\nKeyboardState: {}\n".format(
			self.position, self.doc_len, self.key, self.value, self.keyboardstate)

	def apply_text(self, char_list):
		"""modifies char_list with contents of this event"""
		if self.value == backspace_value:
			if self.position > len(char_list):
				del char_list[-1]
			else:
				del char_list[self.position-1]
			return
		elif not self.value or len(self.value) != 1:
			return
		char_list.insert(self.position, unicode(self.value))


class ReplacementEvent(Event):
	def __init__(self, id_, start_time_, end_time_, range_start_, range_end_, newtext_):
		super(ReplacementEvent, self).__init__(id_, start_time_, end_time_)
		self.range_start = int(range_start_)
		self.range_end = int(range_end_)
		self.newtext = newtext_
		assert(self.range_start >= 0)
		assert(len(self.newtext) == self.range_end - self.range_start)

	def __str__(self):
		return type(self).__name__ + "\n" + super(ReplacementEvent, self).__str__() + "\nRange: {}-{}\nNew Text: {}".format(
				self.range_start, self.range_end, self.newtext)

	def apply_text(self, char_list):
		"""modifies char list to contain the new text from the replacement"""

		del char_list[self.range_start:self.range_end]
		for i, elt in enumerate(self.newtext):
			char_list.insert(self.position + i, elt)


def make_event(event_node):
	"""Possibly unnecessary object-orientation-ness.
	   Maintenance point for addng new event types."""

	type = event_node.attrib["type"]
	if type == "keyboard":
		if len(event_node) > 1:
			return KeyboardEvent(
			event_node.attrib["id"],
			position_=event_node.find("part/position").text,
			doc_len_=event_node.find("part/documentLength").text,
			start_time_=event_node.find("part/startTime").text,
			end_time_=event_node.find("part/endTime").text,
			key_=event_node.find("part/key").text,
			value_=event_node.find("part/value").text,
			keyboardstate_=event_node.find("part/keyboardstate").text )
	elif type == "replacement":
		return ReplacementEvent(
		event_node.attrib["id"],
		start_time_=None,
		end_time_=None,
		range_start_=event_node.find("part/start").text,
		range_end_=event_node.find("part/end").text,
		newtext_=event_node.find("part/newtext").text, )
	else:
		return None

def get_event_stream(filename):
	"""returns tuple (session_meta, [events])"""
	tree = ET.parse(data_location + filename)
	root = tree.getroot() # log node
	# first child is <meta>
	# then <session>
	meta = root[0]
	session = root[1]

	session = Session_Meta(
		name_ = session[0][1].text,
		language_ = session[1][1].text,
		age_ = session[2][1].text,
		gender_ = session[3][1].text,
		session_ = session[4][1].text,
		end_time_ = root.findall("event//endTime")[-1].text # gets the last occurrence of "endTime", but maybe we just want the last keystroke event time?
		)
	#print(session)

	# all others are <event> nodes until the end
	events = []
	focus = True # MS word has focus
	for event in root[2:]:
		# parse each event node

		# ignore event nodes when out of focus
		if event.attrib["id"] == "focus":
			if event[0][0].text == "Microsoft Office Word":
				focus = True
			else:
				focus = False
		if not focus:
			continue

		try:
			new_event = make_event(event)
		except TypeError:
			continue # just ignore other ill-formed events not otherwise dealt with explicitly
		if new_event:
			events.append(new_event)
	return (session, events)

def read_all_files():
	"""returns dictionary {filename -> (session_meta, [events])}"""
	d = {}
	for filename in os.listdir(data_location):
		print("Reading file: {}".format(filename))
		try:
			d[filename] = get_event_stream(filename)
		except (IOError, ET.ParseError) as e:
			print("Could not parse file: {}".format(filename))
			continue
	return d
